package id.co.telkomsigma.shipping.controller;

import java.math.BigDecimal;
import java.util.Map;
import java.util.Random;
import java.util.TreeMap;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author <a href="mailto:ridla.fadilah@gmail.com">Ridla Fadilah</a>
 */
@RestController
public class ShippingCostApiController {

    private Random random = new Random();

    // http://app/api/cost/jkt/bgr/12
    @GetMapping("/api/cost/{asal}/{tujuan}/{berat}")
    public Map<String, String> hitungBiaya(@PathVariable String asal,
                                           @PathVariable String tujuan,
                                           @PathVariable BigDecimal berat){
        Map<String, String> hasil = new TreeMap<>();
        hasil.put("asal", asal);
        hasil.put("tujuan", tujuan);
        hasil.put("berat", berat.toString());
        hasil.put("reguler", String.valueOf(random.nextInt(100000)+10000));
        hasil.put("kilat", String.valueOf(random.nextInt(200000)+20000));
        return hasil;
    }
}
